# flow.py
# Data structures and functions common to maximum-flow algorithms.

import sys
from typing import Mapping, List
from lib import binary_search

def augment_with_path(graph, path: List[int], flow: int):
    """Pushes a flow along the given augmenting path in the graph."""
    for i in range(len(path) - 1):
        src = path[i]
        dst = path[i+1]
        graph.push(src, dst, flow)


def load_residual_graph_from_stdin_of_type(constructor):
    print("Reading graph from stdin...", file=sys.stderr)
    
    text = sys.stdin.read()
    lines = text.splitlines()
    num_vertices = int(lines[0])
    num_edges = int(lines[1])
    
    graph = constructor(num_vertices)
    
    for i in range(2, len(lines)):
        line = lines[i]
        src, dst, capacity = (int(n) for n in line.split())
        graph.add_edge(src, dst, capacity)
    
    print("Done!", file=sys.stderr)
    return graph


class MapResidualGraph:
    """A residual graph in which neighbors are identified by a map to capacities
    of edges to neighbors, and an order to iterate them in"""
    def __init__(self, num_vertices: int):
        self.edges = [({}, []) for _ in range(num_vertices)]
        self.num_vertices = num_vertices
    
    def add_edge(self, src: int, dst: int, capacity: int):
        """Adds an edge to the graph. Adds an empty back-edge with 
        capacity 0, if a back-edge is not defined. O(1)"""
        src_edges, src_order = self.edges[src]
        if not dst in src_edges:
            src_order.append(dst)
        
        src_edges[dst] = capacity
        
        dst_edges, dst_order = self.edges[dst]
        if not src in dst_edges:
            dst_edges[src] = 0
            dst_order.append(src)
    
    def push(self, src: int, dst: int, flow: int):
        """Pushes flow from one vertex to another. O(1)"""
        self.edges[src][0][dst] -= flow
        self.edges[dst][0][src] += flow
    
    def capacity(self, src: int, dst: int):
        """Returns the capacity of the given edge."""
        return self.edges[src][0].get(dst)
    
    def edge_from(self, src: int, index: int):
        """Returns the 'index'th edge from/to this node. O(1)"""
        src_edges, src_order = self.edges[src]
        if index >= len(src_order):
            return (None, None)
        else:
            dst = src_order[index]
            return (dst, src_edges[dst])
    
    def pprint(self):
        print("Graph:")
        for i in range(self.num_vertices):
            edges, order = self.edges[i]
            print("  {}: {}".format(i, [(dst, edges[dst]) for dst in order]))
    
        print("")


class BinListResidualGraph:
    """A residual graph using a binary searched list for neighbors, meaning
    that accesses of a particular neighbor is O(log(occ))"""
    def __init__(self, num_vertices):
        self.num_vertices = num_vertices
        self.edges = [[] for _ in range(num_vertices)]
    
    def add_edge(self, src: int, dst: int, capacity: int):
        """Adds an edge to the graph. Inserts a back-edge with capacity 0 if
        none exists. O(log(occ)) where occ is the number of neighbors"""
        edges = self.edges[src]
        (idx, res) = binary_search(edges, dst, lambda e: e[0])
        if res is None:
            edges.insert(idx, (dst, capacity))
        else:
            edges[idx] = (dst, capacity)
        
        # Always add a back-edge
        dst_edges = self.edges[dst]
        (dst_idx, res) = binary_search(dst_edges, src, lambda e: e[0])
        if res is None:
            dst_edges.insert(dst_idx, (src, 0))
    
    def push(self, src: int, dst: int, flow: int):
        """Pushes flow from one vertex to another. O(log(occ))"""
        src_edges = self.edges[src]
        (src_i, (_, src_cap)) = binary_search(src_edges, dst, lambda e: e[0])
        src_edges[src_i] = (dst, src_cap - flow)
        
        dst_edges = self.edges[dst]
        (dst_i, (_, dst_cap)) = binary_search(dst_edges, src, lambda e: e[0])
        dst_edges[dst_i] = (src, dst_cap + flow)
    
    def edge_from(self, src: int, index: int):
        """Returns the 'index'th edge from/to this node or None. O(log(occ))"""
        edges = self.edges[src]
        if index >= len(edges):
            return (None, None)
        else:
            return edges[index]
    
    def pprint(self):
        print("Graph:")
        for i in range(self.num_vertices):
            print("  {}: {}".format(i, self.edges[i]))
    
        print("")
        


