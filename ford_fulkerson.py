# ford_fulkerson.py
# An implementation of the Ford-Fulkerson max-flow algorithm

import sys

from flow import MapResidualGraph
from flow import load_residual_graph_from_stdin_of_type


def augment_with_dfs_stack(graph, stack) -> int:
    # This step of removing comparisons and shrinking the stack on the DFS
    # was an improvement in the Rust impl, but seemed to pessimize the perf
    # slightly in this Python version, probably due to instruction overhead and
    # less cache-sensitivity.
    flow = sys.maxsize
    for i in range(len(stack) - 1):
        src = stack[i][0]
        dst = stack[i+1][0]
        flow = min(flow, graph.capacity(src, dst))
        #print("flow {} -> {} = {}".format(src, dst, flow))
    
    for i in range(len(stack) - 1):
        src = stack[i][0]
        dst = stack[i+1][0]
        graph.push(src, dst, flow)
    
    return flow


def find_augmenting_path(graph, source, sink):
    stack = [(source, 0)]
    visited = set()
    visited.add(source)
    while stack:
        src, nid = stack.pop()
        #print("Checking {}...".format(src))
        dst, cap = graph.edge_from(src, nid)
        while dst is not None:
            nid += 1
            if cap == 0 or dst in visited:
                dst, cap = graph.edge_from(src, nid)
                continue
            
            #print("  {} -> {}: cap {}".format(src, dst, cap))
            visited.add(dst)
            stack.append((src, nid))
            stack.append((dst, 0))
            if dst == sink:
                return stack
            
            break
                
    return None


def find_maximum_flow(graph, source, sink):
    max_flow = 0
    stack = find_augmenting_path(graph, source, sink)
    while stack is not None:
        flow = augment_with_dfs_stack(graph, stack)
        #print("Found flow of {}: {}".format(flow, stack))
        max_flow += flow
        stack = find_augmenting_path(graph, source, sink)
    
    return max_flow


def main():
    graph = load_residual_graph_from_stdin_of_type(MapResidualGraph)
    SOURCE = 0
    SINK = 1
    max_flow = find_maximum_flow(graph, SOURCE, SINK)
    print("Max flow:", file=sys.stderr)
    print(max_flow)


if __name__ == '__main__':
    main()

