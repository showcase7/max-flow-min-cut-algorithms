# lib.py
# Commonly used functions for algorithms.

def binary_search(l: list, value, cmpval, offset=0):
    """Returns an insertion spot, and the value in that position, if it
    matches the sought value."""
    #print("binary_search {} in {}".format(value, l))
    if len(l) == 0:
        return (offset, None)
    elif len(l) == 1:
        #print("  Trivial: {}".format(l))
        cmp = cmpval(l[0])
        if cmp == value:
            return (offset, l[0])
        elif cmp < value:
            return (offset + 1, None)
        else:
            return (offset, None)
    
    middle = len(l) // 2
    cmp_mid = cmpval(l[middle])
    #print("  middle(|{}|) = l[{}] = {}".format(len(l), middle, cmp_mid))
    if cmp_mid == value:
        return (offset + middle, l[middle])
    elif cmp_mid < value:
        end = offset + len(l)
        #print("  .. | value [{} : {}]".format(offset + middle + 1, end))
        offset = offset + middle + 1
        return binary_search(l[middle + 1 :], value, cmpval, offset=offset)
    else:
        #print("  value | .. [{} : {}]".format(offset, offset + middle))
        return binary_search(l[: middle], value, cmpval, offset=offset)



def test_binary_search():
    a = [1, 2, 3, 5, 6, 8, 10, 11, 13] # 9 numbers, 0:8
    cmp = lambda v: v
    print("Searching for 5")
    res = binsearch(a, 5, cmp)
    assert res == (3, 5), "Search for 5 failed"
    print("")
    
    print("Searching for 7")
    res = binsearch(a, 7, cmp)
    assert res == (5, None), "Search for 7 failed"
    print("")
    
    print("Searching for 14")
    res = binsearch(a, 14, cmp)
    assert res == (9, None), "Search for 14 failed"
    print("")
    
    print("Searching for 0")
    res = binsearch(a, 0, cmp)
    assert res == (0, None), "Search for 0 failed"
    print("")
    
    print("Searching for 2985")
    edges = sorted([(2675, 4), (3725, 0), (9582, 2), (9285, 1), (9858, 0)])
    res = binsearch(edges, 9285, lambda e: e[0])
    assert res == (2, (9285, 1)), "Search for 2985 failed"
    print("")
