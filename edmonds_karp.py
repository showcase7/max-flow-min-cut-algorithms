# edmonds_karp.py
# An implementation of the Edmonds-Karp maximum-flow algorithm.

import sys
from typing import Mapping, List
from flow import MapResidualGraph, BinListResidualGraph
from flow import load_residual_graph_from_stdin_of_type
from flow import augment_with_path

def find_shortest_augmenting_path(graph, source: int, sink: int, 
        visited: Mapping[int, int], stack: List[int], path: List[int]
        ) -> (List[int], int):
    visited.clear()
    stack.clear()
    stack.append((source, sys.maxsize))
    visited[source] = None
    sink_reached = False
    path_flow = 0
    
    while stack:
        active, flow = stack.pop()
        #print("  Active: {}, flow {}".format(active, flow), file=sys.stderr)
        
        # Check all outgoing edges from the active vertex
        i = 0
        dst, cap = graph.edge_from(active, i)
        while dst is not None:
            #print("    {} -> {} [cap {}]".format(active, dst, cap), file=sys.stderr)
            
            # If we haven't visited this vertex, add it to the queue
            if dst not in visited and cap != 0:
                
                # Constrain the flow by the capacity of this connection
                dst_flow = min(flow, cap)
                visited[dst] = active
                stack.append((dst, dst_flow))
                if dst == sink:
                    # Update the stats and break the outer loop
                    sink_reached = True
                    path_flow = dst_flow
                    stack.clear()
                    break
            
            i += 1
            dst, cap = graph.edge_from(active, i)
    
    if not sink_reached:
        # Return tuple for easier unwrapping
        return (None, None)
    
    # Find a way back from the sink
    path.clear()
    path.append(sink)
    prev = visited[sink]
    while prev is not None:
        path.append(prev)
        prev = visited[prev]
    
    # Make sure that the path goes in the right direction
    path.reverse()
    return (path, path_flow)


def find_maximum_flow(graph, source: int, sink: int) -> int:
    visited = {}
    stack = []
    path = []
    total_flow = 0
    while True:
        path, flow = find_shortest_augmenting_path(graph, source, sink, visited, stack, path)
        if not path:
            break
        
        #print("  Found path: flow {} -> {}".format(flow, path), file=sys.stderr)
        augment_with_path(graph, path, flow)
        total_flow += flow
    
    return total_flow


def main():
    resgraph_2 = False
    
    args = sys.argv[1:]
    for arg in args:
        if arg == "--rg2":
            resgraph_2 = True
        else:
            print("Unknown argument: {}".format(arg), file=sys.stderr)
    
    if resgraph_2:
        graph_type = MapResidualGraph
    else:
        graph_type = BinListResidualGraph
    
    graph = load_residual_graph_from_stdin_of_type(graph_type)
    
    SOURCE = 0
    SINK = 1
    
    #graph.pprint()
    
    max_flow = find_maximum_flow(graph, SOURCE, SINK)
    print("Max flow:", file=sys.stderr)
    print(max_flow)
    

if __name__ == '__main__':
    main()




