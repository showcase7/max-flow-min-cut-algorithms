# Max-flow min-cut algorithms
## Description
These are a few implementations of algorithms to solve the max-cut min-flow problem, that I made for a graduate course on algorithms and data structures.

The repository contains implementations of the following algorithms:
- Ford and Fulkerson's algorithm in `ford_fulkerson.py`
- Edmonds and Karp's algorithm in `edmonds_karp.py`
- Dinitz' algorithm in `dinitz.py`

## Building and running the programs
The programs require a Python interpreter for Python 3.5 or greater.

The programs expect to receive an encoded flow-graph in standard input (a directed graph with each edge labeled with a capacity). Vertex 0 is the designated source and vertex 1 is the designated sink. An example of this graph format can be seen in `example_graph.txt`.

The programs can be run like this:

`$ python3 dinitz.py < example_graph.txt`

## License
This project is available under the Universal Permissive License, Version 1.0 ([LICENSE.txt]) or https://oss.oracle.com/licenses/upl/ .
