# dinitz.py
# An implementation of Dinitz' max-flow algorithm

from flow import MapResidualGraph
from flow import load_residual_graph_from_stdin_of_type
from collections import deque
import sys


def augment_graph_with_prev(graph, source, sink, prev):
    """Augments the graph using a list of previous vertices and their capacities,
    but starting from the sink and going to the source."""
    cur = source
    flow = sys.maxsize
    path = []
    # Append to the path so that we exclude the source and include the sink
    while cur != sink:
        cur, cap = prev[cur]
        path.append(cur)
        flow = min(flow, cap)
    
    src = source
    for dst in path:
        graph.push(src, dst, flow)
        src = dst
    
    return flow


# Opt: Find at least one augmenting path while doing this step ;)
def calculate_levels_into(graph, source, sink, level):
    """Do a BFS from the sink to the source, finding the distance from the 
    source of all nodes in the path. Also returns whether a path from the
    source to the sink was found."""
    
    # Reset the levels
    for i in range(graph.num_vertices):
        level[i] = -1
    
    level[sink] = 0
    queue = deque()
    queue.append((sink, 1))
    prev = {}
    while queue:
        src, neighbor_dist = queue.popleft()
        i = 0
        dst, cap = graph.edge_from(src, i)
        while dst is not None:
            i += 1
            # Ignore visited nodes
            if level[dst] != -1:
                dst, cap = graph.edge_from(src, i)
                continue
            
            in_cap = graph.capacity(dst, src)
            if in_cap != 0:
                prev[dst] = (src, in_cap)
                # If the source is reached
                if dst == source:
                    # All of the vertices in the queue have had their level set,
                    # so any of the ones connected to the source should be okay.
                    level[source] = neighbor_dist
                    
                    # Use the map of predecessors to add an augmenting path :p
                    bfs_flow = augment_graph_with_prev(graph, source, sink, prev)
                    
                    # This is no longer necessarily true. But at worst we'll 
                    # try one more time.
                    return bfs_flow
                
                level[dst] = neighbor_dist
                queue.append((dst, neighbor_dist + 1))
                
            dst, cap = graph.edge_from(src, i)
    
    # No connection from the sink to the source was found
    return 0


def find_augmenting_path(graph, source, sink, level, state=None):
    """Finds an augmenting path using the given levels, and potentially
    reusing a previous DFS state. Returns the state of the stack and the set
    of visited vertices, or (None, None) if no path was found.
    If a stack is returned, the top item will be the sink and how much flow
    it received from this path."""
    if state is None:
        # stack: vertex id, inflow to vertex, next neighbor idx to check
        stack = [(source, sys.maxsize, 0)]
        visited = set()
    else:
        stack, visited = state
    
    while stack:
        src, inflow, nid = stack.pop()
        dst, cap = graph.edge_from(src, nid)
        next_level = level[src] - 1
        while dst is not None:
            nid += 1
            if level[dst] == next_level and dst not in visited and cap != 0:
                # Schedule a check of the next neighbor later
                stack.append((src, inflow, nid))
                flow = min(inflow, cap)
                stack.append((dst, flow, 0))
                
                if dst == sink:
                    return (stack, visited)
                else:
                    # Don't add the sink to visited nodes.
                    visited.add(dst)
            
            dst, cap = graph.edge_from(src, nid)
    
    return (None, None)


def backtrack_to_first_blocked(stack, flow):
    """Updates a DFS stack with flow having been pushed through it, and
    removes all the vertices after the first newly blocked edge."""
    if not stack:
        return
    
    # Update the inflow into the vertices on the stack
    for i, (vertex, inflow, nid) in enumerate(stack):
        new_flow = inflow - flow
        # If the vertex is blocked, remove it along with all vertices further
        # on the path
        if new_flow == 0:
            del stack[i:]
            break
        
        stack[i] = (vertex, new_flow, nid)


def augment_graph_with_stack(graph, stack, flow):
    """Pushes flow through an augmenting path in the graph (here represented as 
    a DFS stack)."""
    #print("Augmenting path of {} flow:".format(flow))
    src = stack[0][0]
    for i, (dst, _, _) in enumerate(stack):
        if i == 0: continue
        #print("  {} -> {}".format(src, dst))
        graph.push(src, dst, flow)
        src = dst
    #print("")


def find_maximum_flow(graph, source, sink) -> int:
    """Finds the maximum flow in the graph between the source and the sink,
    using Dinic's algorithm. This algorithm runs in O(|V||E|) in the general
    case. For more info, see the paper 
    "Dinitz' algorithm: The original version and Even's version""""
    
    max_flow = 0
    level = [-1 for _ in range(graph.num_vertices)]
    
    while True:
        bfs_flow = calculate_levels_into(graph, source, sink, level)
        
        # The BFS found that source and sink was no longer connected
        if not bfs_flow:
            break
        else:
            max_flow += bfs_flow
        
        #print("Layer graph:")
        #for vertex, dist in enumerate(level):
        #    if dist == -1: continue
        #    print("  {}: {}".format(vertex, dist))
        #print("")
        
        #print("Looking for path...")
        stack, visited = find_augmenting_path(graph, source, sink, level)
        
        while stack:
            flow = stack[-1][1]
            max_flow += flow
            #print("  Applying path...")
            augment_graph_with_stack(graph, stack, flow)
            
            # Resume the search
            #print("  Backtracking...")
            backtrack_to_first_blocked(stack, flow)
            if stack:
                #print("  Looking for another path...")
                state = (stack, visited)
                stack, visited = find_augmenting_path(graph, source, sink, level, state=state)
    
    return max_flow


def main():
    graph = load_residual_graph_from_stdin_of_type(MapResidualGraph)
    
    SOURCE = 0
    SINK = 1
    max_flow = find_maximum_flow(graph, SOURCE, SINK)
    print("Max flow:", file=sys.stderr)
    print(max_flow)


if __name__ == '__main__':
    main()

